# r-pan-scaffolding

#### 介绍

一个个人文件管理系统的源码脚手架，[r-pan](http://demo.pan.rubinchu.com)基于此脚手架搭建，希望对有快速搭建个人文件管理系统的小伙伴有所帮助  

#### 版本 
v5.0

#### 版本更新内容
* 更换了前端上传组件，新增了断点续传、暂停继续上传、拖拽上传等功能
* 优化了上传任务面板UI，新增了上传速度、剩余时间等监控信息的显示
* 优化了文件预览UI，支持多文件同时预览
* 优化了上传效率，使用多线程保证了更快的上传速度
* 优化了文件落盘方式（NIO）
* 修复了一些BUG

#### 后续版本迭代方向
* 优化网站UI
* 优化定时任务模块，集成现有比较流行的定时任务框架
* 优化上传，支持文件夹上传等等
* 优化下载，支持断点继续下载和文件夹打包下载等等
* 优化分享功能，加入聊天以及好友系统
* 优化登录注册，支持第三方登录等等
* 敬请期待...

#### 网站图片
![输入图片说明](https://images.gitee.com/uploads/images/2020/0705/210618_7ae41201_1506368.png "WX20200705-204847@2x.png")
![输入图片说明](https://images.gitee.com/uploads/images/2020/0705/210628_247662dd_1506368.png "WX20200705-204906@2x.png")
![输入图片说明](https://images.gitee.com/uploads/images/2020/0705/210753_589c72c7_1506368.png "WX20200705-204925@2x.png")
![输入图片说明](https://images.gitee.com/uploads/images/2021/0415/183751_d5e53ee7_1506368.png "index.png")
![输入图片说明](https://gitee.com/RubinChu/r-pan-scaffolding/raw/v5.0/imgs/upload.png "upload.png")
![输入图片说明](https://gitee.com/RubinChu/r-pan-scaffolding/raw/v5.0/imgs/task-list.png "task-list.png")
![输入图片说明](https://images.gitee.com/uploads/images/2021/0415/183913_a617ed19_1506368.png "recycle.png")
![输入图片说明](https://images.gitee.com/uploads/images/2021/0415/183939_28ea83e3_1506368.png "create-share.png")
![输入图片说明](https://images.gitee.com/uploads/images/2021/0415/183951_b2bf6f2b_1506368.png "create-share-success.png")
![输入图片说明](https://images.gitee.com/uploads/images/2021/0415/184010_f2e3a06c_1506368.png "share-list.png")
![输入图片说明](https://images.gitee.com/uploads/images/2021/0415/184031_9b1add15_1506368.png "share-code.png")
![输入图片说明](https://images.gitee.com/uploads/images/2021/0415/184042_800e098f_1506368.png "share-front.png")
![输入图片说明](https://images.gitee.com/uploads/images/2021/0415/184055_a0ee67f8_1506368.png "share-expire.png")

#### 软件架构

SpringBoot 2.x 后台基本框架  
Mybaits 传统的ORM框架  
MySQL 数据库
Redis 缓存中间件   
druid 数据库连接池    
Vue2 + ElementUI 前端基础框架   

#### 细节文档
该项目细节的文档请移步[WIKI](https://gitee.com/RubinChu/r-pan-scaffolding/wikis/%E5%BF%AB%E9%80%9F%E5%BC%80%E5%A7%8B?sort_id=4381391)

#### 写在最后

本项目会持续更新，希望对有此方面需求的小伙伴尽一点绵薄之力。有问题可以私信作者或者加群交流。  

![输入图片说明](R%20PAN%E4%BA%A4%E6%B5%81%E7%BE%A4%E4%B8%80%E7%BE%A4%E7%BE%A4%E4%BA%8C%E7%BB%B4%E7%A0%81.png)